package com.appiumair.controller;

import java.util.concurrent.CountDownLatch;

/**
 * 控制并发线程，等待所有线程执行完，然后，加载本地JS文件
 */
public interface CountDownLatchLocalJS {
    public static CountDownLatch countDownLatch = new CountDownLatch(AppiumTest.androidInfoMap.size());
}
